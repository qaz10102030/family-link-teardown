package com.google.android.apps.kids.familylinkhelper.accountselector;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import defpackage.bjm;
import defpackage.bjw;
import defpackage.bjx;
import defpackage.bjy;
import defpackage.bjz;
import defpackage.bkh;
import defpackage.bkr;
import defpackage.bks;
import defpackage.buo;
import defpackage.ctl;
import defpackage.fvp;
import defpackage.fwk;
import defpackage.fwl;
import defpackage.fwp;
import defpackage.fwq;
import defpackage.ggp;
import defpackage.ghz;
import defpackage.glj;
import defpackage.hcc;
import defpackage.hyc;
import defpackage.i;

/* compiled from: PG */
public final class LoginAccountSelectorActivity extends bkh implements fvp, fwk {
    private final ggp k = new ggp(this);
    private boolean l;
    private Context m;
    private final long n = SystemClock.elapsedRealtime();
    private boolean o;
    private bjz p;
    private i q;

    private final void r() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.DecodeException: Load method exception in method: com.google.android.apps.kids.familylinkhelper.accountselector.LoginAccountSelectorActivity.r():void
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:116)
	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:249)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: java.lang.NullPointerException
*/
        /*
        r0 = this;
        r0 = r4.p;
        if (r0 != 0) goto L_0x0068;
    L_0x0004:
        r0 = r4.l;
        if (r0 == 0) goto L_0x0060;
    L_0x0008:
        r0 = r4.o;
        if (r0 == 0) goto L_0x001b;
    L_0x000c:
        r0 = r4.isFinishing();
        if (r0 == 0) goto L_0x0013;
        goto L_0x001b;
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called after destroyed.";
        r0.<init>(r1);
        throw r0;
        r0 = "CreateComponent";
        r0 = defpackage.gjr.a(r0);
        r4.a();	 Catch:{ all -> 0x0056 }
        r0.close();
        r0 = "CreatePeer";
        r0 = defpackage.gjr.a(r0);
        r1 = r4.a();	 Catch:{ ClassCastException -> 0x0044 }
        r1 = (defpackage.bka) r1;	 Catch:{ ClassCastException -> 0x0044 }
        r1 = r1.j();	 Catch:{ all -> 0x0042 }
        r4.p = r1;	 Catch:{ all -> 0x0042 }
        r0.close();
        r0 = r4.p;
        r0.a = r4;
        return;
    L_0x0042:
        r1 = move-exception;
        goto L_0x004d;
    L_0x0044:
        r1 = move-exception;
        r2 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x0042 }
        r3 = "Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.";	 Catch:{ all -> 0x0042 }
        r2.<init>(r3, r1);	 Catch:{ all -> 0x0042 }
        throw r2;	 Catch:{ all -> 0x0042 }
        r0.close();	 Catch:{ all -> 0x0051 }
        goto L_0x0055;
    L_0x0051:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x0056:
        r1 = move-exception;
        r0.close();	 Catch:{ all -> 0x005b }
        goto L_0x005f;
    L_0x005b:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x0060:
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called outside of onCreate";
        r0.<init>(r1);
        throw r0;
    L_0x0068:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.kids.familylinkhelper.accountselector.LoginAccountSelectorActivity.r():void");
    }

    private final bjz t() {
        r();
        return this.p;
    }

    public final i aL() {
        if (this.q == null) {
            this.q = new fwl(this);
        }
        return this.q;
    }

    public final void applyOverrideConfiguration(Configuration configuration) {
        Context baseContext = getBaseContext();
        if (baseContext == null) {
            baseContext = this.m;
        }
        glj.b(baseContext, configuration);
        super.applyOverrideConfiguration(configuration);
    }

    protected final void attachBaseContext(Context context) {
        this.m = context;
        super.attachBaseContext(glj.a(context));
        this.m = null;
    }

    public final void invalidateOptionsMenu() {
        ghz n = ggp.n();
        try {
            super.invalidateOptionsMenu();
            n.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final boolean j() {
        ghz i = this.k.i();
        try {
            boolean j = super.j();
            if (i != null) {
                i.close();
            }
            return j;
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void l() {
    }

    protected final void onActivityResult(int i, int i2, Intent intent) {
        ghz o = this.k.o();
        try {
            super.onActivityResult(i, i2, intent);
            if (o != null) {
                o.close();
            }
        } catch (int i22) {
            hcc.a(i, i22);
        }
    }

    public final void onBackPressed() {
        ghz h = this.k.h();
        try {
            t().a();
            h.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onCreate(Bundle bundle) {
        ghz p = this.k.p();
        try {
            this.l = true;
            r();
            ((fwl) aL()).g(this.k);
            ((fwp) a()).h().a();
            super.onCreate(bundle);
            bjz t = t();
            if (bundle == null) {
                for (PackageInfo packageInfo : t.b.getInstalledPackages(0)) {
                    if (packageInfo.packageName.equals("com.google.android.apps.kids.familylink")) {
                        t.c.a(19);
                    }
                }
                t.b(bks.e(), 0, 0);
            }
            ctl.i(this).b = findViewById(16908290);
            bundle = this.p;
            ctl.d(this, bkr.class, new bjw(bundle));
            ctl.d(this, bjm.class, new bjx(bundle));
            ctl.d(this, buo.class, new bjy(bundle));
            this.l = false;
            if (p != null) {
                p.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    public final boolean onCreatePanelMenu(int i, Menu menu) {
        ghz q = this.k.q();
        try {
            i = super.onCreatePanelMenu(i, menu);
            q.close();
            return i;
        } catch (Menu menu2) {
            hcc.a(i, menu2);
        }
    }

    protected final void onDestroy() {
        ghz g = this.k.g();
        try {
            super.onDestroy();
            this.o = true;
            g.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onNewIntent(Intent intent) {
        ghz a = this.k.a(intent);
        try {
            super.onNewIntent(intent);
            if (a != null) {
                a.close();
            }
        } catch (Throwable th) {
            hcc.a(intent, th);
        }
    }

    public final boolean onOptionsItemSelected(MenuItem menuItem) {
        ghz r = this.k.r();
        try {
            menuItem = super.onOptionsItemSelected(menuItem);
            if (r != null) {
                r.close();
            }
            return menuItem;
        } catch (Throwable th) {
            hcc.a(menuItem, th);
        }
    }

    protected final void onPause() {
        ghz e = this.k.e();
        try {
            super.onPause();
            if (e != null) {
                e.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onPostCreate(Bundle bundle) {
        ghz s = this.k.s();
        try {
            super.onPostCreate(bundle);
            if (s != null) {
                s.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onPostResume() {
        ghz d = this.k.d();
        try {
            super.onPostResume();
            d.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ghz t = this.k.t();
        try {
            super.onRequestPermissionsResult(i, strArr, iArr);
            if (t != null) {
                t.close();
            }
        } catch (String[] strArr2) {
            hcc.a(i, strArr2);
        }
    }

    protected final void onResume() {
        ghz c = this.k.c();
        try {
            super.onResume();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onSaveInstanceState(Bundle bundle) {
        ghz u = this.k.u();
        try {
            super.onSaveInstanceState(bundle);
            if (u != null) {
                u.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onStart() {
        ghz b = this.k.b();
        try {
            super.onStart();
            t().c(6);
            if (b != null) {
                b.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onStop() {
        ghz f = this.k.f();
        try {
            super.onStop();
            t().c(7);
            if (f != null) {
                f.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final long p() {
        return this.n;
    }

    public final /* bridge */ /* synthetic */ hyc q() {
        return fwq.b(this);
    }
}
