package com.google.android.apps.kids.familylinkhelper.accountselector;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import defpackage.bix;
import defpackage.biy;
import defpackage.bke;
import defpackage.fvx;
import defpackage.fwk;
import defpackage.fwo;
import defpackage.hyo;
import defpackage.hyw;

/* compiled from: PG */
public final class AccountItemView extends bke {
    private bix a;

    @Deprecated
    public AccountItemView(Context context) {
        super(context);
        d();
    }

    private final void d() {
        if (this.a == null) {
            try {
                this.a = ((biy) a()).a();
                Context context = getContext();
                while ((context instanceof ContextWrapper) && !(context instanceof hyw) && !(context instanceof hyo) && !(context instanceof fwo)) {
                    context = ((ContextWrapper) context).getBaseContext();
                }
                if (!(context instanceof fwk)) {
                    String cls = getClass().toString();
                    StringBuilder stringBuilder = new StringBuilder(String.valueOf(cls).length() + 57);
                    stringBuilder.append("TikTok View ");
                    stringBuilder.append(cls);
                    stringBuilder.append(", cannot be attached to a non-TikTok Fragment");
                    throw new IllegalStateException(stringBuilder.toString());
                }
            } catch (Throwable e) {
                throw new IllegalStateException("Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.", e);
            }
        }
    }

    public final bix b() {
        bix bix = this.a;
        if (bix != null) {
            return bix;
        }
        throw new IllegalStateException("peer() called before initialized.");
    }

    protected final /* bridge */ /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return generateDefaultLayoutParams();
    }

    protected final /* bridge */ /* synthetic */ LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return generateLayoutParams(layoutParams);
    }

    protected final void onFinishInflate() {
        super.onFinishInflate();
        d();
    }

    public AccountItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AccountItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public AccountItemView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public AccountItemView(fvx fvx) {
        super(fvx);
        d();
    }
}
