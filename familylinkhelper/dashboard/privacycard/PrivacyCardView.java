package com.google.android.apps.kids.familylinkhelper.dashboard.privacycard;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import defpackage.btm;
import defpackage.btn;
import defpackage.btp;
import defpackage.fvx;
import defpackage.fwk;
import defpackage.fwo;
import defpackage.hyo;
import defpackage.hyw;

/* compiled from: PG */
public final class PrivacyCardView extends btp {
    public btm g;

    @Deprecated
    public PrivacyCardView(Context context) {
        super(context);
        d();
    }

    private final void d() {
        if (this.g == null) {
            try {
                this.g = ((btn) a()).e();
                Context context = getContext();
                while ((context instanceof ContextWrapper) && !(context instanceof hyw) && !(context instanceof hyo) && !(context instanceof fwo)) {
                    context = ((ContextWrapper) context).getBaseContext();
                }
                if (!(context instanceof fwk)) {
                    String cls = getClass().toString();
                    StringBuilder stringBuilder = new StringBuilder(String.valueOf(cls).length() + 57);
                    stringBuilder.append("TikTok View ");
                    stringBuilder.append(cls);
                    stringBuilder.append(", cannot be attached to a non-TikTok Fragment");
                    throw new IllegalStateException(stringBuilder.toString());
                }
            } catch (Throwable e) {
                throw new IllegalStateException("Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.", e);
            }
        }
    }

    protected final /* bridge */ /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return generateDefaultLayoutParams();
    }

    protected final void onFinishInflate() {
        super.onFinishInflate();
        d();
    }

    public PrivacyCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PrivacyCardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public PrivacyCardView(fvx fvx) {
        super(fvx);
        d();
    }
}
