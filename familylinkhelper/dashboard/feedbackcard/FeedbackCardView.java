package com.google.android.apps.kids.familylinkhelper.dashboard.feedbackcard;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import defpackage.btg;
import defpackage.bth;
import defpackage.btj;
import defpackage.fvx;
import defpackage.fwk;
import defpackage.fwo;
import defpackage.hyo;
import defpackage.hyw;

/* compiled from: PG */
public final class FeedbackCardView extends btj {
    public btg g;

    @Deprecated
    public FeedbackCardView(Context context) {
        super(context);
        d();
    }

    private final void d() {
        if (this.g == null) {
            try {
                this.g = ((bth) a()).d();
                Context context = getContext();
                while ((context instanceof ContextWrapper) && !(context instanceof hyw) && !(context instanceof hyo) && !(context instanceof fwo)) {
                    context = ((ContextWrapper) context).getBaseContext();
                }
                if (!(context instanceof fwk)) {
                    String cls = getClass().toString();
                    StringBuilder stringBuilder = new StringBuilder(String.valueOf(cls).length() + 57);
                    stringBuilder.append("TikTok View ");
                    stringBuilder.append(cls);
                    stringBuilder.append(", cannot be attached to a non-TikTok Fragment");
                    throw new IllegalStateException(stringBuilder.toString());
                }
            } catch (Throwable e) {
                throw new IllegalStateException("Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.", e);
            }
        }
    }

    protected final /* bridge */ /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return generateDefaultLayoutParams();
    }

    protected final void onFinishInflate() {
        super.onFinishInflate();
        d();
    }

    public FeedbackCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FeedbackCardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FeedbackCardView(fvx fvx) {
        super(fvx);
        d();
    }
}
