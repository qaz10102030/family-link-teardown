package com.google.android.apps.kids.familylinkhelper.dashboard.semanticlocationcard;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import defpackage.btv;
import defpackage.btw;
import defpackage.bty;
import defpackage.fvx;
import defpackage.fwk;
import defpackage.fwo;
import defpackage.hyo;
import defpackage.hyw;

/* compiled from: PG */
public final class SemanticLocationCardView extends bty {
    private btv g;

    @Deprecated
    public SemanticLocationCardView(Context context) {
        super(context);
        f();
    }

    private final void f() {
        if (this.g == null) {
            try {
                this.g = ((btw) a()).f();
                Context context = getContext();
                while ((context instanceof ContextWrapper) && !(context instanceof hyw) && !(context instanceof hyo) && !(context instanceof fwo)) {
                    context = ((ContextWrapper) context).getBaseContext();
                }
                if (!(context instanceof fwk)) {
                    String cls = getClass().toString();
                    StringBuilder stringBuilder = new StringBuilder(String.valueOf(cls).length() + 57);
                    stringBuilder.append("TikTok View ");
                    stringBuilder.append(cls);
                    stringBuilder.append(", cannot be attached to a non-TikTok Fragment");
                    throw new IllegalStateException(stringBuilder.toString());
                }
            } catch (Throwable e) {
                throw new IllegalStateException("Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.", e);
            }
        }
    }

    private final btv h() {
        f();
        return this.g;
    }

    public final btv c() {
        btv btv = this.g;
        if (btv != null) {
            return btv;
        }
        throw new IllegalStateException("peer() called before initialized.");
    }

    protected final /* bridge */ /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return generateDefaultLayoutParams();
    }

    protected final void onAttachedToWindow() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.DecodeException: Load method exception in method: com.google.android.apps.kids.familylinkhelper.dashboard.semanticlocationcard.SemanticLocationCardView.onAttachedToWindow():void
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:116)
	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:249)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: java.lang.NullPointerException
*/
        /*
        r0 = this;
        super.onAttachedToWindow();
        r0 = r12.h();
        r0.e();
        r1 = r0.m;
        r1 = r0.d(r1);
        if (r1 != 0) goto L_0x0013;
    L_0x0012:
        return;
    L_0x0013:
        r1 = r0.b;
        r2 = r0.m;
        r2 = r2.d;
        if (r2 != 0) goto L_0x001d;
    L_0x001b:
        r2 = defpackage.hlw.h;
    L_0x001d:
        r2 = r2.c;
        r4 = new btu;
        r4.<init>(r0);
        r6 = defpackage.gja.d(r4);
        r4 = new iwv;
        r7 = java.lang.System.currentTimeMillis();
        r4.<init>(r2, r7);
        r2 = defpackage.dcb.a;
        r3 = r4.b;
        r5 = defpackage.dcb.a;
        r7 = r5.b;
        r3 = r3 % r7;
        r3 = defpackage.iwv.b(r3);
        r3 = r3.b;
        r7 = 0;
        r5 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1));
        if (r5 == 0) goto L_0x0058;
    L_0x0046:
        r5 = -1;
        r3 = defpackage.iyy.c(r3, r5);
        r7 = r2.b;
        r2 = defpackage.iyy.a(r7, r3);
        r4 = new iwv;
        r4.<init>(r2);
        r2 = r4;
        goto L_0x0059;
        r5 = r1.b;
        r7 = r2.b;
        r1 = defpackage.dcb.a;
        r9 = r1.b;
        r11 = java.util.concurrent.TimeUnit.MILLISECONDS;
        r1 = r5.scheduleWithFixedDelay(r6, r7, r9, r11);
        r0.l = r1;
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.kids.familylinkhelper.dashboard.semanticlocationcard.SemanticLocationCardView.onAttachedToWindow():void");
    }

    protected final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        h().e();
    }

    protected final void onFinishInflate() {
        super.onFinishInflate();
        f();
    }

    public SemanticLocationCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SemanticLocationCardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public SemanticLocationCardView(fvx fvx) {
        super(fvx);
        f();
    }
}
