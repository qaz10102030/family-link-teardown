package com.google.android.apps.kids.familylinkhelper.accountrequirements.flmdownload;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import defpackage.bgy;
import defpackage.bhc;
import defpackage.bhd;
import defpackage.bhv;
import defpackage.bib;
import defpackage.bik;
import defpackage.do;
import defpackage.fvp;
import defpackage.fwk;
import defpackage.fwl;
import defpackage.fwp;
import defpackage.fwq;
import defpackage.ggp;
import defpackage.ghz;
import defpackage.glj;
import defpackage.hcc;
import defpackage.hyc;
import defpackage.i;

/* compiled from: PG */
public final class FlmDownloadActivity extends bik implements fvp, fwk {
    private bgy k;
    private final ggp l = new ggp(this);
    private boolean m;
    private Context n;
    private final long o = SystemClock.elapsedRealtime();
    private boolean p;
    private i q;

    private final void r() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.DecodeException: Load method exception in method: com.google.android.apps.kids.familylinkhelper.accountrequirements.flmdownload.FlmDownloadActivity.r():void
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:116)
	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:249)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: java.lang.NullPointerException
	at jadx.core.dex.nodes.MethodNode.addJump(MethodNode.java:370)
	at jadx.core.dex.nodes.MethodNode.initJumps(MethodNode.java:356)
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:106)
	... 7 more
*/
        /*
        r0 = this;
        r0 = r4.k;
        if (r0 != 0) goto L_0x0068;
    L_0x0004:
        r0 = r4.m;
        if (r0 == 0) goto L_0x0060;
    L_0x0008:
        r0 = r4.p;
        if (r0 == 0) goto L_0x001b;
    L_0x000c:
        r0 = r4.isFinishing();
        if (r0 == 0) goto L_0x0013;
        goto L_0x001b;
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called after destroyed.";
        r0.<init>(r1);
        throw r0;
        r0 = "CreateComponent";
        r0 = defpackage.gjr.a(r0);
        r4.a();	 Catch:{ all -> 0x0056 }
        r0.close();
        r0 = "CreatePeer";
        r0 = defpackage.gjr.a(r0);
        r1 = r4.a();	 Catch:{ ClassCastException -> 0x0044 }
        r1 = (defpackage.bha) r1;	 Catch:{ ClassCastException -> 0x0044 }
        r1 = r1.c();	 Catch:{ all -> 0x0042 }
        r4.k = r1;	 Catch:{ all -> 0x0042 }
        r0.close();
        r0 = r4.k;
        r0.c = r4;
        return;
    L_0x0042:
        r1 = move-exception;
        goto L_0x004d;
    L_0x0044:
        r1 = move-exception;
        r2 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x0042 }
        r3 = "Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.";	 Catch:{ all -> 0x0042 }
        r2.<init>(r3, r1);	 Catch:{ all -> 0x0042 }
        throw r2;	 Catch:{ all -> 0x0042 }
        r0.close();	 Catch:{ all -> 0x0051 }
        goto L_0x0055;
    L_0x0051:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x0056:
        r1 = move-exception;
        r0.close();	 Catch:{ all -> 0x005b }
        goto L_0x005f;
    L_0x005b:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x0060:
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called outside of onCreate";
        r0.<init>(r1);
        throw r0;
    L_0x0068:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.kids.familylinkhelper.accountrequirements.flmdownload.FlmDownloadActivity.r():void");
    }

    public final i aL() {
        if (this.q == null) {
            this.q = new fwl(this);
        }
        return this.q;
    }

    public final void applyOverrideConfiguration(Configuration configuration) {
        Context baseContext = getBaseContext();
        if (baseContext == null) {
            baseContext = this.n;
        }
        glj.b(baseContext, configuration);
        super.applyOverrideConfiguration(configuration);
    }

    protected final void attachBaseContext(Context context) {
        this.n = context;
        super.attachBaseContext(glj.a(context));
        this.n = null;
    }

    public final void invalidateOptionsMenu() {
        ghz n = ggp.n();
        try {
            super.invalidateOptionsMenu();
            n.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final boolean j() {
        ghz i = this.l.i();
        try {
            boolean j = super.j();
            if (i != null) {
                i.close();
            }
            return j;
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void l() {
    }

    protected final void onActivityResult(int i, int i2, Intent intent) {
        ghz o = this.l.o();
        try {
            super.onActivityResult(i, i2, intent);
            if (o != null) {
                o.close();
            }
        } catch (int i22) {
            hcc.a(i, i22);
        }
    }

    public final void onBackPressed() {
        ghz h = this.l.h();
        try {
            r();
            bgy bgy = this.k;
            do x = bgy.a.e().x(16908290);
            if (x instanceof bhc) {
                bhd d = ((bhc) x).d();
                do x2 = d.b.D().x(2131362096);
                if (x2 instanceof bib) {
                    d.d.a(207);
                } else if (x2 instanceof bhv) {
                    d.d.a(208);
                }
                d.c.b();
            }
            if (bgy.b.c()) {
                bgy.b.d();
            } else {
                bgy.b.b();
            }
            h.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onCreate(Bundle bundle) {
        ghz p = this.l.p();
        try {
            this.m = true;
            r();
            ((fwl) aL()).g(this.l);
            ((fwp) a()).h().a();
            super.onCreate(bundle);
            this.m = null;
            if (p != null) {
                p.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    public final boolean onCreatePanelMenu(int i, Menu menu) {
        ghz q = this.l.q();
        try {
            i = super.onCreatePanelMenu(i, menu);
            q.close();
            return i;
        } catch (Menu menu2) {
            hcc.a(i, menu2);
        }
    }

    protected final void onDestroy() {
        ghz g = this.l.g();
        try {
            super.onDestroy();
            this.p = true;
            g.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onNewIntent(Intent intent) {
        ghz a = this.l.a(intent);
        try {
            super.onNewIntent(intent);
            if (a != null) {
                a.close();
            }
        } catch (Throwable th) {
            hcc.a(intent, th);
        }
    }

    public final boolean onOptionsItemSelected(MenuItem menuItem) {
        ghz r = this.l.r();
        try {
            menuItem = super.onOptionsItemSelected(menuItem);
            if (r != null) {
                r.close();
            }
            return menuItem;
        } catch (Throwable th) {
            hcc.a(menuItem, th);
        }
    }

    protected final void onPause() {
        ghz e = this.l.e();
        try {
            super.onPause();
            if (e != null) {
                e.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onPostCreate(Bundle bundle) {
        ghz s = this.l.s();
        try {
            super.onPostCreate(bundle);
            if (s != null) {
                s.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onPostResume() {
        ghz d = this.l.d();
        try {
            super.onPostResume();
            d.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ghz t = this.l.t();
        try {
            super.onRequestPermissionsResult(i, strArr, iArr);
            if (t != null) {
                t.close();
            }
        } catch (String[] strArr2) {
            hcc.a(i, strArr2);
        }
    }

    protected final void onResume() {
        ghz c = this.l.c();
        try {
            super.onResume();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onSaveInstanceState(Bundle bundle) {
        ghz u = this.l.u();
        try {
            super.onSaveInstanceState(bundle);
            if (u != null) {
                u.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onStart() {
        ghz b = this.l.b();
        try {
            super.onStart();
            if (b != null) {
                b.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onStop() {
        ghz f = this.l.f();
        try {
            super.onStop();
            if (f != null) {
                f.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final long p() {
        return this.o;
    }

    public final /* bridge */ /* synthetic */ hyc q() {
        return fwq.b(this);
    }
}
