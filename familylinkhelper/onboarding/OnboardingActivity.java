package com.google.android.apps.kids.familylinkhelper.onboarding;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import defpackage.bxw;
import defpackage.bye;
import defpackage.byr;
import defpackage.dxp;
import defpackage.fvp;
import defpackage.fwk;
import defpackage.fwl;
import defpackage.fwp;
import defpackage.fwq;
import defpackage.ggp;
import defpackage.ghz;
import defpackage.glj;
import defpackage.hcc;
import defpackage.hyc;
import defpackage.i;

/* compiled from: PG */
public final class OnboardingActivity extends bye implements fvp, fwk {
    private bxw k;
    private final ggp l = new ggp(this);
    private boolean m;
    private Context n;
    private final long o = SystemClock.elapsedRealtime();
    private boolean p;
    private i q;

    private final void r() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.DecodeException: Load method exception in method: com.google.android.apps.kids.familylinkhelper.onboarding.OnboardingActivity.r():void
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:116)
	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:249)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: java.lang.NullPointerException
*/
        /*
        r0 = this;
        r0 = r4.k;
        if (r0 != 0) goto L_0x0064;
    L_0x0004:
        r0 = r4.m;
        if (r0 == 0) goto L_0x005c;
    L_0x0008:
        r0 = r4.p;
        if (r0 == 0) goto L_0x001b;
    L_0x000c:
        r0 = r4.isFinishing();
        if (r0 == 0) goto L_0x0013;
        goto L_0x001b;
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called after destroyed.";
        r0.<init>(r1);
        throw r0;
        r0 = "CreateComponent";
        r0 = defpackage.gjr.a(r0);
        r4.a();	 Catch:{ all -> 0x0052 }
        r0.close();
        r0 = "CreatePeer";
        r0 = defpackage.gjr.a(r0);
        r1 = r4.a();	 Catch:{ ClassCastException -> 0x0040 }
        r1 = (defpackage.bxx) r1;	 Catch:{ ClassCastException -> 0x0040 }
        r1 = r1.f();	 Catch:{ all -> 0x003e }
        r4.k = r1;	 Catch:{ all -> 0x003e }
        r0.close();
        return;
    L_0x003e:
        r1 = move-exception;
        goto L_0x0049;
    L_0x0040:
        r1 = move-exception;
        r2 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x003e }
        r3 = "Missing entry point. If you're in a test with explicit entry points specified in your @TestRoot, check that you're not missing the one for this class.";	 Catch:{ all -> 0x003e }
        r2.<init>(r3, r1);	 Catch:{ all -> 0x003e }
        throw r2;	 Catch:{ all -> 0x003e }
        r0.close();	 Catch:{ all -> 0x004d }
        goto L_0x0051;
    L_0x004d:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x0052:
        r1 = move-exception;
        r0.close();	 Catch:{ all -> 0x0057 }
        goto L_0x005b;
    L_0x0057:
        r0 = move-exception;
        defpackage.hcc.a(r1, r0);
        throw r1;
    L_0x005c:
        r0 = new java.lang.IllegalStateException;
        r1 = "createPeer() called outside of onCreate";
        r0.<init>(r1);
        throw r0;
    L_0x0064:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.kids.familylinkhelper.onboarding.OnboardingActivity.r():void");
    }

    public final i aL() {
        if (this.q == null) {
            this.q = new fwl(this);
        }
        return this.q;
    }

    public final void applyOverrideConfiguration(Configuration configuration) {
        Context baseContext = getBaseContext();
        if (baseContext == null) {
            baseContext = this.n;
        }
        glj.b(baseContext, configuration);
        super.applyOverrideConfiguration(configuration);
    }

    protected final void attachBaseContext(Context context) {
        this.n = context;
        super.attachBaseContext(glj.a(context));
        this.n = null;
    }

    public final void invalidateOptionsMenu() {
        ghz n = ggp.n();
        try {
            super.invalidateOptionsMenu();
            n.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final boolean j() {
        ghz i = this.l.i();
        try {
            boolean j = super.j();
            if (i != null) {
                i.close();
            }
            return j;
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void l() {
    }

    protected final void onActivityResult(int i, int i2, Intent intent) {
        ghz o = this.l.o();
        try {
            super.onActivityResult(i, i2, intent);
            if (o != null) {
                o.close();
            }
        } catch (int i22) {
            hcc.a(i, i22);
        }
    }

    public final void onBackPressed() {
        ghz h = this.l.h();
        try {
            super.onBackPressed();
            h.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onCreate(Bundle bundle) {
        ghz p = this.l.p();
        try {
            this.m = true;
            r();
            ((fwl) aL()).g(this.l);
            ((fwp) a()).h().a();
            super.onCreate(bundle);
            r();
            bxw bxw = this.k;
            if (bundle == null) {
                bundle = bxw.a;
                bundle.e = byr.g;
                bundle.d.a(dxp.h(null), "OnboardingDataService");
            }
            this.m = null;
            if (p != null) {
                p.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    public final boolean onCreatePanelMenu(int i, Menu menu) {
        ghz q = this.l.q();
        try {
            i = super.onCreatePanelMenu(i, menu);
            q.close();
            return i;
        } catch (Menu menu2) {
            hcc.a(i, menu2);
        }
    }

    protected final void onDestroy() {
        ghz g = this.l.g();
        try {
            super.onDestroy();
            this.p = true;
            g.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onNewIntent(Intent intent) {
        ghz a = this.l.a(intent);
        try {
            super.onNewIntent(intent);
            if (a != null) {
                a.close();
            }
        } catch (Throwable th) {
            hcc.a(intent, th);
        }
    }

    public final boolean onOptionsItemSelected(MenuItem menuItem) {
        ghz r = this.l.r();
        try {
            menuItem = super.onOptionsItemSelected(menuItem);
            if (r != null) {
                r.close();
            }
            return menuItem;
        } catch (Throwable th) {
            hcc.a(menuItem, th);
        }
    }

    protected final void onPause() {
        ghz e = this.l.e();
        try {
            super.onPause();
            if (e != null) {
                e.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onPostCreate(Bundle bundle) {
        ghz s = this.l.s();
        try {
            super.onPostCreate(bundle);
            if (s != null) {
                s.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onPostResume() {
        ghz d = this.l.d();
        try {
            super.onPostResume();
            d.close();
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ghz t = this.l.t();
        try {
            super.onRequestPermissionsResult(i, strArr, iArr);
            if (t != null) {
                t.close();
            }
        } catch (String[] strArr2) {
            hcc.a(i, strArr2);
        }
    }

    protected final void onResume() {
        ghz c = this.l.c();
        try {
            super.onResume();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onSaveInstanceState(Bundle bundle) {
        ghz u = this.l.u();
        try {
            super.onSaveInstanceState(bundle);
            if (u != null) {
                u.close();
            }
        } catch (Throwable th) {
            hcc.a(bundle, th);
        }
    }

    protected final void onStart() {
        ghz b = this.l.b();
        try {
            super.onStart();
            if (b != null) {
                b.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    protected final void onStop() {
        ghz f = this.l.f();
        try {
            super.onStop();
            if (f != null) {
                f.close();
            }
        } catch (Throwable th) {
            hcc.a(th, th);
        }
    }

    public final long p() {
        return this.o;
    }

    public final /* bridge */ /* synthetic */ hyc q() {
        return fwq.b(this);
    }
}
